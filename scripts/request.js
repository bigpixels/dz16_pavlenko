"use strict";

let request = new XMLHttpRequest();

const api = 'https://swapi.co/api';
const starshipsAPI = `${api}/starships`;

let starshipsObj = {};
request.open('GET', starshipsAPI, true);

let promise = new Promise(function (resolve, reject) {
    request.onreadystatechange = function () {
        if(this.readyState === 4) {
            if(this.status === 200) {
                let strResponse = this.response;
                starshipsObj = JSON.parse(strResponse);
                resolve(starshipsObj["results"]);
            } else {
                reject(this.responseText);
            }
        }
    };
});

request.send();

promise.then(function (result) {
    console.log(result);
}).catch(function(e) {
    console.log(e);
});